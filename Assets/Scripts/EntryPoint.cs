﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TSP.ForanjMatch3.Core;

public class EntryPoint : MonoBehaviour
{
	[SerializeField]
	private GameObject _rootCanvas = null;

	private TimeManager _timeManager = null;
	private WindowsManager _windowsManager = null;
    private SceneLoader _sceneLoader = null;
    private Scoreboar _scoreboar = null;

	private void Awake()
	{
		_timeManager = new TimeManager();
		_windowsManager = new WindowsManager();
        _sceneLoader = new SceneLoader();
        _scoreboar = new Scoreboar();

    }

	private void Start()
	{
		_timeManager.Setup();
		_windowsManager.SetupRootCanvas(_rootCanvas);
        _sceneLoader.Setup();
        _scoreboar.Setup();

        _sceneLoader.LoadScene(SceneID.Menu);
	}

	private void Update()
	{
		_timeManager.Update();
		_windowsManager.Update();
        _sceneLoader.Update();
        _scoreboar.Update();
	}
}
