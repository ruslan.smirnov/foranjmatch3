﻿using UnityEngine;
using System.Collections;

// https://indiehoodgames.wordpress.com/2013/07/27/pixel-perfect-calculator-for-orthographic-camera-unity3d/ =>
// => orthographicSize = y / 2 * s =>
// => Camera.main.orthographicSize = Screen.height / (2.0f * pixelPerUnit); where pixelPerUnit = 100

namespace TSP.ForanjMatch3.Utility
{
    public sealed class CameraRescaler : MonoBehaviour
    {
        [SerializeField]
        private float _defaultWidth = 1920.0f;

        [SerializeField]
        private float _defaultPixelsPerUnit = 100.0f;

        private void Awake()
        {
            float width = Screen.width;
            float height = Screen.height;

            if (_defaultWidth <= width)
            {
                return;
            }

            float widthRadio = _defaultWidth / width;
            float scalePixelsPerUnit = _defaultPixelsPerUnit / widthRadio;
            float result = height / (2.0f * scalePixelsPerUnit);

            Camera.main.orthographicSize = result;
        }
    }
}