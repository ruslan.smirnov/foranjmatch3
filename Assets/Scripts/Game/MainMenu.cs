﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TSP.ForanjMatch3.Core;

namespace TSP.ForanjMatch3.Game
{
    public sealed class MainMenu : MonoBehaviour
    {
        [SerializeField]
        private Button _playButton = null;

        [SerializeField]
        private Button _scoreboardButton = null;

        [SerializeField]
        private Button _aboutButton = null;

        [SerializeField]
        private Button _exitButton = null;

        private void OnPlayButtonClick()
        {
            SceneLoader.Instance.LoadScene(SceneID.Game);
        }

        private void OnScoreboardButtonClick()
        {

        }

        private void OnAboutButtonClick()
        {

        }

        private void OnExitButtonClick()
        {

        }

        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButtonClick);
            _scoreboardButton.onClick.AddListener(OnScoreboardButtonClick);
            _aboutButton.onClick.AddListener(OnAboutButtonClick);
            _exitButton.onClick.AddListener(OnExitButtonClick);
        }

        private void OnDestroy()
        {
            _playButton.onClick.RemoveAllListeners();
            _scoreboardButton.onClick.RemoveAllListeners();
            _aboutButton.onClick.RemoveAllListeners();
            _exitButton.onClick.RemoveAllListeners();
        }
    }
}

