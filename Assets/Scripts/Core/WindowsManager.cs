﻿using System;
using System.Collections.Generic;

using UnityEngine;
using Object = UnityEngine.Object;

using TSP.ForanjMatch3.Game.Windows;

namespace TSP.ForanjMatch3.Core
{
    public sealed class WindowsManager : DataManager<WindowsManager>
    {
        private GameObject _root;

        private readonly Dictionary<Type, string> _windows = new Dictionary<Type, string>()
        {
			//{ typeof(DefeatWindow), "Prefabs/Windows/DefeatWindow" },
			//{ typeof(VictoryWindow), "Prefabs/Windows/VictoryWindow" },
		};

        public WindowsManager()
        {

        }

        public void SetupRootCanvas(GameObject gameObject)
        {
            _root = gameObject;
        }

        public T Show<T>() where T : WindowAbstract
        {
            string path = string.Empty;
            _windows.TryGetValue(typeof(T), out path);

            var prefab = Resources.Load<GameObject>(path);
            var gameObject = Object.Instantiate(prefab);
            var window = gameObject.GetComponent<T>();

            window.transform.SetParent(_root.transform, false);
            window.Setup();
            window.Show();

            return window;
        }

		public void Update()
		{

		}
    }
}

