﻿using System;

namespace TSP.ForanjMatch3.Core
{
    public sealed class Timer
    {
        public float Time { get; private set; }
        public bool IsRunned { get; private set; }

        public Action Complete;

        public Timer(float time)
        {
            Time = time;
            IsRunned = false;
        }

        public void Run()
        {
            IsRunned = true;
        }

        public void Reset(float time)
        {
            IsRunned = false;
            Time = time;
        }

        public void Update()
        {
            TimeManager tm = TimeManager.Instance;

            if (IsRunned && !tm.IsPaused)
            {
                Time -= tm.CurrentDeltaTime;

                if (Time <= 0)
                {
                    IsRunned = false;
                    Complete?.Invoke();
                }
            }
        }
    }
}

