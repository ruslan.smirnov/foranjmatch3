﻿using UnityEngine;
using System.Collections.Generic;

namespace TSP.ForanjMatch3.Core
{
    public sealed class TimeManager : DataManager<TimeManager>
    {
        public bool IsPaused { get; private set; }
        public float CurrentDeltaTime { get { return IsPaused ? 0.0f : Time.deltaTime * _currentDeltaTime; } }

        private readonly List<Timer> _timers = new List<Timer>();

        private float _currentDeltaTime = 1.0f;

        public TimeManager()
        {

        }

        public void Setup()
        {

        }

        public Timer RunTimer(float time)
        {
            var timer = new Timer(time);
            timer.Run();

            _timers.Add(timer);

            return timer;
        }

        public void Pause()
        {
            IsPaused = true;
        }

        public void UnPause()
        {
            IsPaused = false;
        }

        public void IncTime()
        {
            _currentDeltaTime += 1.0f;
        }

        public void DecTime()
        {
            if (_currentDeltaTime > 0)
            {
                _currentDeltaTime -= 1.0f;
            }
        }

        public void Update()
        {
            if (!IsPaused)
            {
                var listUpdating = new List<Timer>(_timers);
                var listToDelete = new List<Timer>();

                foreach(Timer timer in listUpdating)
                {
                    timer.Update();

                    if (!timer.IsRunned)
                    {
                        listToDelete.Add(timer);
                    }
                }

                foreach(Timer timer in listToDelete)
                {
                    _timers.Remove(timer);
                }

                listToDelete.Clear();
            }
        }
    }
}

