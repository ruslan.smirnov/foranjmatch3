# ForanjMatch3

Тестовое задание на MACH-3

Должны быть реализованы 4 сцены

## 1. Сцена “Главный экран”

Содержит:

* Новая игра - переходит на вторую сцену (с геймплеем)
* Таблица рекордов - переходит на третью сцену (с рекордами)
* О программе - переходит на четвёртую сцену (с описанием)
* Выход - Выводит подтверждающее окно с кнопками “Выйти” и “Остаться”. 

При нажатии на кнопку “Выйти” приложение закрывается.
При нажатии на кнопку  “Остаться” окно закрывается.


## 2. Сцена “Геймплей”

Содержит:

* Игровое поле
* Начало игры:
	В начале игры должна быть показана механика игры.
	Подсветить первый ход.

* Игровой процесс
	Тач на “шарик” он взрывается. Списывается один ход. 
    Если Совпадает три одинаковых шарика они взываются, начисляется 2 хода. 
    Если взрываются 4 шарика начисляется 3 хода, 5-4. 
    Вышестоящие шарики сдвигаются вниз.

* кнопку “в меню”. При нажатии:
	Игра переходит в режим паузы
	Выводится окно с предложением выйти в основное меню 
и кнопками “В меню” и “Остаться”
	При нажатии на кнопку “В меню”  выполняется переход на первую сцену.
	При нажатии на кнопку “Остатсья” игра оживает
* набранное количество очков
* Оставшееся количество ходов

##### Окончание игры

Игра завершается если нет возможных ходов или закончилось количество ходов
Проверяется количество набранных очков. 
Если игрок попадает в таблицу рекордов, то выполняется переход на третью сцену с выделением внесённой строки.
Если не набрано необходимое количество очков, то выдается грустное сообщение с кнопкой “ОК” при нажатии на которую выполняется переход на первую сцену.

## 3. Сцена “Таблица рекордов”

Содержит:

* Список игр с датами в порядке убывания количества очков.
* Кнопку “в меню” при нажатии выполняется переход на первую сцену
* При первом запуске программы таблица рекордов заполняется из csv-файла

## 4. Сцена “О программе”

Содержит:

* Rраткое описание программы и краткое руководство к игре. 
* А так же активную ссылку на профиль разработчика в одной из социальных сетей.

Предполагается что будут использованы
●	Unity3D (есть бесплатная версия)
●	Язык программирования C#
●	Одна из парадигм паттернового программирования использующихся в создании игр. Рекомендуется к прочтению “Game Programming Patterns”
Оригинал http://gameprogrammingpatterns.com/
Перевод http://live13.livejournal.com/462582.html
●	Сцены unity
●	Префабы - для игровых объектов
●	UserPrefs - для хранения данных таблицы рекордов
●	Интерфейсные элементы можно использовать встроенные Unity3D
●	Простейшая анимация взрыва шарика (возможна любая другая, то как уменьшение, вылет итд)
●	Простейшая оптимизация работы с игровыми объектами

С целью оптимизации после взрыва шарика объект не уничтожать, а просто переносить на новое место.

Вспомогательные материалы
1. Движок Unity доступен бесплатно без регистрации
http://unity3d.com/unity/download

2. Обучалка Unity 4.3 - 2D Game Development Walkthrough
http://www.youtube.com/watch?v=4qE8cuHI93c#t=189

3. Презинтация Unite 2013 - New 2D Workflows
http://www.youtube.com/watch?v=B1F6fi04qw8
